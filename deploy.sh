sshpass  -p $1 ssh -o StrictHostKeyChecking=no $7 << ENDSSH
   docker login -u $2 -p $3 $4
   docker stop $5
   docker rm $5
   docker pull $6
   docker run --restart=always -d --name $5 -p 1234:80 $6
ENDSSH